package customer;


public class Client {
	
		
	private final String idC;
	private final String originalChannelC;
	private final String fullNameC;
	private final String conditionC;
	private final String segmentC;
	private final String addressC;
	private final String customerCategoryC;
	
	
	
	public Client(String id,String originalChannel,	String fullName, String condition, String segment, 	String address, String customerCategory) {
        idC = id;
        originalChannelC = originalChannel;
        fullNameC=fullName;
        conditionC=condition;
        segmentC =segment;
        addressC =address;
        customerCategoryC=customerCategory;
        
    }
	
	public String getId() {
		return idC;
	}

	public String getOriginalChannel() {
		return originalChannelC;
	}

	public String getFullName() {
		return fullNameC;
	}

	public String getCondition() {
		return conditionC;
	}

	public String getSegment() {
		return segmentC;
	}

	public String getAddress() {
		return addressC;
	}

	public String getCustomerCategory() {
		return customerCategoryC;
	}


}
