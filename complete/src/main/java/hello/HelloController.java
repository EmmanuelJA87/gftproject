package hello;

import org.springframework.web.bind.annotation.RestController;
import customer.Client;
import exceptions.CustomerNotFoundException;
import exceptions.InternalServerErrorException;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.web.bind.annotation.ResponseBody;

import org.springframework.web.client.RestTemplate;


@RestController
public class HelloController {
	
	private final  Hashtable<String, Client> customersData = new Hashtable<String, Client>();
	private static final Logger log = LoggerFactory.getLogger(Application.class);
	
	
	private RestTemplateBuilder builder=new RestTemplateBuilder(); 
	private RestTemplate restTemplate=builder.build();
	


    @RequestMapping("/")
    public String index() {
        return "Hola EmmanuelJA";
    }
	
	
    @RequestMapping(value="/customer",
			  method = RequestMethod.GET/*, 
			  produces = " text/plain; charset=utf-8"*/)
	@ResponseBody
    public Map<String, String>  getCustomer(@RequestParam String customer_id) throws InternalServerErrorException {
			Hashtable<String, Map<String, String>> customer =restTemplate.getForObject("http://localhost:8080/client", Hashtable.class) ;
			
			Map<String, String> response = new HashMap<String, String>();
			
			boolean flag=false;
			for (Map.Entry<String, Map<String, String>> entry : customer.entrySet()) {
				log.info("clave=" + entry.getKey() + ", valor=" + entry.getValue());
				String llave=entry.getKey();
				log.info("llave "+llave);
				Map<String, String> c= entry.getValue();
				log.info("ID. "+c.get("id"));
				if(c.get("id").equals(customer_id) /*&& flag==false*/){
				    log.info("Encontrado "+c.get("id"));
				    flag=true;
				    response.putAll(c);
				    return response;
				}		    
			}
			if (!flag) {
				throw new CustomerNotFoundException("Customer Not Found. ");
			}
			//return response;
			return response;
	}
	
	
/*
 * Data Service
 * Contiene todos los datos 
 * 
 * */	
	
	@RequestMapping("/client")
	public Hashtable<String, Client> client(@RequestParam(value="customer_id", defaultValue="00021244") String customer_id) {
		Client customerDetail = new Client("00021244","Banca SERFIN","Aceves Gonzales Adolfo","Cliente","Premier","Loma Bonita 1839","Fisica");
		customersData.put("1", customerDetail);
		Client customerDetail2 = new Client("00021257","Banca BBV","Emmanuel JA","Cliente","Premier","Miraflores 934","Logic");
		customersData.put("2", customerDetail2);
		Client customerDetail3 = new Client("00021278","HSBC","JOSE","Master","Premium","SanJuan ","N/A");
		customersData.put("3", customerDetail3);
	    return customersData;
	}
	
	
	
	
}
